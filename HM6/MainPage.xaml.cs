﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Net.Http;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;



namespace HM6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()  // Hello ! I can't lauch my app because of a problem with Hyper-v and I don't know how to resolve it.... I hope the app will launch on your laptop...
        {
            InitializeComponent();
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet) //check if internet is ok
            {   
                DisplayAlert("OK", "Internet is ok", "ok"); //print the correct response
            }
            else
            {
                DisplayAlert("NOT OK", "Internet is not ok", "ok");
            }
            GetDico(); //call GetDisco to call the Api
        }

        public partial class Welcome //model for the response
        {
            [JsonProperty("type")]
            public string type { get; set; }

            [JsonProperty("definition")]
            public string definition { get; set; }

            [JsonProperty("example")]
            public string example { get; set; }
        }


        async void GetDico() // GetDico will call the api owlbot api v2
        {
            HttpClient client = new HttpClient();

            var uri = new Uri(string.Format("https://owlbot.info/api/v2/dictionary/cat")); //set up the correct uri

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get; // I choose to GET information
            request.RequestUri = uri; // call the api
            //request.Headers.Add("Authorizati: on", "fd13b3f5f741e72c812611144b1e8ff544b6f269");

            HttpResponseMessage response = await client.SendAsync(request); // reponse = the return of the call
            Welcome myData = null;
            if (response.IsSuccessStatusCode) //if the status code is 200 
            {
                var content = await response.Content.ReadAsStringAsync(); // content = information of the api
                DisplayAlert("CAT", content , "OK"); // Display the return of the request
                //var Data = JsonConvert.DeserializeObject<Welcome>(content);
                //Console.WriteLine(myData.example);
                //WordList.ItemsSource = myData;
            }
            else
            {
                DisplayAlert("Error", "There is a problem", "ok"); //Display an erro
            }
        }
    }
}
